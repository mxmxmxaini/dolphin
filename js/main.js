var canvas = document.getElementById("canvas");
var clock = new THREE.Clock();
var _start = false;
var mixer_bike, mixer_dolphin;
var clips_dolphin, clips_bike;
var _delay = 500;
var screenDimensions = {
    width: canvas.width,
    height: canvas.height
}

var dolphin, bike;

var scene = buildScene();

var renderer = buildRender(screenDimensions);

var camera = buildCamera(screenDimensions);
// var controls = buildControls();

createSceneObjects(scene);

bindEventListeners();

function buildScene() {
    const scene = new THREE.Scene();
    scene.background = new THREE.Color("rgb(255,255,255)");
    return scene;
}

function buildControls() {
    const orbitControl = new THREE.OrbitControls(camera, renderer.domElement);
    return orbitControl;
}

// load gltf
function loadModel(url, name) {

    const loader = new THREE.GLTFLoader();

    loader.load(url, function (gltf) {

        var model = gltf.scene;


        model.traverse(function (child) {
            if (child.isMesh) {
                // child.castShadow = true;
                // child.receiveShadow = true;
            }
        });

        switch (name) {
            case 'bike':
                model.scale.set(50, 50, 50);
                model.position.z += 9
                model.position.y -= 1.5;
                bike = model;
                bike.visible = false;
                clips_bike = gltf.animations;
                mixer_bike = new THREE.AnimationMixer(model);
                for (var i = 0; i < clips_bike.length; ++i) {
                    mixer_bike.clipAction(clips_bike[i]).play();
                }
                break;
            case 'dolphin':
                model.scale.set(10, 10, 10);
                model.position.set(0, -3, 0);
                dolphin = model;
                model.rotation.y = Math.PI / 2;
                clips_dolphin = gltf.animations;
                mixer_dolphin = new THREE.AnimationMixer(model);
                break;
        }
        scene.add(model);
        // mixer.clipAction(clips[1].optimize()).play();
    }, undefined, function (e) {
        console.error(e);
    });
    render()
}

function buildRender({ width, height }) {
    const renderer = new THREE.WebGLRenderer({ canvas: canvas, antialias: true, alpha: true });
    const DPR = (window.devicePixelRatio) ? window.devicePixelRatio : 1;
    renderer.setPixelRatio(DPR);
    renderer.setSize(width, height);
    renderer.shadowMap.enabled = true;
    return renderer;
}

function buildCamera({ width, height }) {
    const aspectRatio = width / height;
    const fieldOfView = 45;
    const nearPlane = 0.1;
    const farPlane = 1000;
    const camera = new THREE.PerspectiveCamera(fieldOfView, aspectRatio, nearPlane, farPlane);
    // position and point the camera to the center of the scene
    camera.position.set(-3.5103263619899443, 4.113119036414232, -25.27032763253516);
    camera.rotation.set(2.9977471841041647, 0.037633636695194664, 3.1361429170432014);
    scene.add(camera);
    return camera;
}

function resizeCanvas() {
    canvas.style.width = '100%';
    canvas.style.height = '100%';

    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
    onWindowResize();
}

function onWindowResize() {
    const { width, height } = canvas;

    screenDimensions.width = width;
    screenDimensions.height = height;

    camera.aspect = width / height;
    camera.updateProjectionMatrix();

    renderer.setSize(width, height);
}

function bindEventListeners() {
    window.onresize = resizeCanvas;
    resizeCanvas();
}



function render() {
    // console.log("Postion \n x:" + camera.position.x + ",y:" + camera.position.y + ",z:" + camera.position.z);
    // console.log("Rotation \n x:" + camera.rotation.x + ",y:" + camera.rotation.y + ",z:" + camera.rotation.z);
    var mixerUpdateDelta = clock.getDelta();
    if (mixer_bike != undefined) {
        mixer_bike.update(mixerUpdateDelta);
    }
    if (mixer_dolphin != undefined) {
        mixer_dolphin.update(mixerUpdateDelta);
        if (!_start) dolpin_anim();
    }
    renderer.render(scene, camera);
    // controls.update();
    TWEEN.update();
    requestAnimationFrame(render);
}

function createSceneObjects() {
    // show axes in the screen
    const axes = new THREE.AxesHelper(20);
    // scene.add(axes);
    var plane = new THREE.Mesh(new THREE.PlaneGeometry(50, 50),
        new THREE.MeshPhongMaterial({ color: 0xffffee }));

    plane.rotation.x = -Math.PI / 2;

    var gridHelper = new THREE.GridHelper(60, 30);
    scene.add(gridHelper);

    scene.add(plane);
    buildLight();

    loadModel('model/dolphin.glb', 'dolphin');
    loadModel('model/bike.glb', 'bike');

}

function buildLight() {
    scene.add(new THREE.AmbientLight(0xffffff, 1));
    scene.add(new THREE.HemisphereLight(0xffffbb, 0x080820, 1));
    scene.add(new THREE.DirectionalLight(0xffffff, 1));
    pointLight = new THREE.PointLight(0xffffff, 1);
    camera.add(pointLight);
}

function dolpin_anim() {
    _start = true;
    camera.position.set(-3.5103263619899443, 1.13119036414232, -25.27032763253516);
    camera.rotation.set(2.9977471841041647, 0.037633636695194664, 3.1361429170432014);
    var _move = new TWEEN.Tween(camera.rotation).to({ y: 0.1 }, 2000).start()
    dolphin.visible = true;
    for (var i = 0; i < clips_dolphin.length; ++i) {
        mixer_dolphin.clipAction(clips_dolphin[i]).play();
    }
    setTimeout(() => {
        for (var i = 0; i < clips_dolphin.length; ++i) {
            mixer_dolphin.clipAction(clips_dolphin[i]).stop();
        }
        bike_anim();
    }, 3500)
}

function bike_anim() {
    camera.position.set(8.492306676715563, 0.5627248832151313, -11.61700943972728);
    camera.rotation.set(-3.114503870755995, 0.6690223922314069, 3.124789135380132);
    bike.visible = true;
    var _move1 = new TWEEN.Tween(camera.position).to({ x: 4.69175788637149, y: 3.474433929013788, z: -11.853391601359034 }, 5000)
        .easing(TWEEN.Easing.Sinusoidal.Out)
        .start()
    var _rot1 = new TWEEN.Tween(camera.rotation).to({ x: -3.1337220674826725, y: 0.09251297876472721, z: 3.140865545531322 }, 5000)
        .easing(TWEEN.Easing.Sinusoidal.Out)
        .start()

    var _move2 = new TWEEN.Tween(camera.position).to({ x: -5.110897886438434, y: 4.291644446615299, z: -5.1262828372952685 }, 5000)
        .easing(TWEEN.Easing.Sinusoidal.Out);
    var _rot2 = new TWEEN.Tween(camera.rotation).to({ x: -2.9336251633446944, y: -0.8797968172814122, z: 6.28 - 2.980390801710314 }, 5000)
        .easing(TWEEN.Easing.Sinusoidal.Out);
    _move1.chain(_move2, _rot2);

    var _move3 = new TWEEN.Tween(camera.position).to({ x: -24.692346955072107, y: 13.982792093143482, z: -11.494559838293391 }, 5000)
        .easing(TWEEN.Easing.Sinusoidal.Out)
    var _rot3 = new TWEEN.Tween(camera.rotation).to({ x: -2.393023696877567, y: -0.95294074821119, z: 6.28 + -2.4935062028377004 }, 5000)
        .easing(TWEEN.Easing.Sinusoidal.Out)
        .onComplete(() => {
            bike.visible = false
            dolpin_anim()
        });
    _move2.chain(_move3, _rot3);
}
